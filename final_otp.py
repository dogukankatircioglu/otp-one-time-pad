#!/usr/bin/env python3
import os, sys       # do not use any other imports/libraries

def bn(b):
    # converts bytes to integer
    i = 0
    for byte in b:
        i <<= 8
        i |= byte
    return i

def nb_len(i, length):
    b = b""
    for _ in range(length):
        b = bytes([i & 0b11111111]) + b
        i = i >> 8
    return b



def encrypt(pfile, kfile, cfile):
    # your implementation here
    with open(pfile, "rb") as plaintext_file:
        plaintext_bytes = plaintext_file.read()
        len_plaintext = len(plaintext_bytes)
        plaintext_integer = bn(plaintext_bytes)
    
    key_bytes = os.urandom(len_plaintext)
    key_integer = bn(key_bytes)

    ciphertext_integer = plaintext_integer ^ key_integer
    ciphertext_bytes = nb_len(ciphertext_integer, len_plaintext)

    with open(kfile, 'wb') as key_file:
        key_file.write(key_bytes)

    with open(cfile, 'wb') as ciphertext_file:
        ciphertext_file.write(ciphertext_bytes)
        

    
def decrypt(cfile, kfile, pfile):
    # your implementation here
    with open(cfile, "rb") as ciphertext_file:
        ciphertext_bytes = ciphertext_file.read()
        len_ciphertext = len(ciphertext_bytes)
        ciphertext_integer = bn(ciphertext_bytes)

    with open(kfile, "rb") as key_file:
        key_bytes = key_file.read()
        key_integer = bn(key_bytes)

    plaintext_integer = ciphertext_integer ^ key_integer
    plaintext_bytes = nb_len(plaintext_integer, len_ciphertext)

    with open(pfile, 'wb') as plaintext_file:
        plaintext_file.write(plaintext_bytes)
    


def usage():
    print("Usage:")
    print("encrypt <plaintext file> <output key file> <ciphertext output file>")
    print("decrypt <ciphertext file> <key file> <plaintext output file>")
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()